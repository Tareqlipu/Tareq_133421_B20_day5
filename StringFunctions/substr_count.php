<?php

$var1 = substr_count("abcdef");    // returns "f"
$var2 = substr_count("abasdfasdfcdef");    // returns "ef"
$var3 = substr_count("abcdesasdf", -3, 1); // returns "d"

echo "$var1";
echo "<hr>";
echo "$var2";
echo "<hr>";
echo "$var3";


?>