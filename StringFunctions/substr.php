<?php

$var1 = substr("abcdef", -1);    // returns "f"
$var2 = substr("abcdef", -2);    // returns "ef"
$var3 = substr("abcdef", -3, 1); // returns "d"

echo "$var1";
echo "<hr>";
echo "$var2";
echo "<hr>";
echo "$var3";


?>