<?php
/**
 * nl2br — Inserts HTML line breaks before all newlines in a string
 * Returns string with '<br />' or '<br>' inserted before all newlines (\r\n, \n\r, \n and \r).
 */

echo nl2br("Welcome\r\nThis is my HTML document", false);
echo "<hr>";

echo nl2br("Hello!\r\n PHP World", false);


?>