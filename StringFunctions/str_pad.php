<?php
$input = "Alien";
echo str_pad($input, 10);                      // produces "Alien     "
echo "<hr>";
echo str_pad($input, 10, "-=", STR_PAD_LEFT);  // produces "-=-=-Alien"
echo "<hr>";
echo str_pad($input, 10, "_", STR_PAD_BOTH);   // produces "__Alien___"
echo "<hr>";
echo str_pad($input,  6, "___");               // produces "Alien_"
echo "<hr>";
echo str_pad($input,  3, "*");                 // produces "Alien"
?>