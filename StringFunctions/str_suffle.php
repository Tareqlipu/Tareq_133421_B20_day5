<?php
$str = 'abcdef';
$shuffled = str_shuffle($str);

// This will echo something like: bfdaec
echo $shuffled;

echo "<hr>";

$var = '123456';
$var1 = str_shuffle($var);
echo $var1;
echo "<hr>";

$var2 = "A52D69K";
echo str_shuffle($var2);


?>