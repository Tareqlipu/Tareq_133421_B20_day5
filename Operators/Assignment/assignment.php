<?php
$a=50;
$b=10;

echo ($a += $b)."<br>";     // $a = $a + $b   //Addition
echo ($a -= $b)."<br>";    //$a = $a - $b     //Subtraction
echo ($a *= $b)."<br>";   //$a = $a * $b     //Multiplication
echo ($a /= $b)."<br>";    //$a = $a / $b    //Division
echo ($a %= $b)."<br>";   //$a = $a % $b    //Modulus
echo ($a .= $b)."<br>";   //$a = $a . $b      //ncatenate
echo ($a &= $b)."<br>";   //$a = $a & $b     //Bitwise And
echo ($a |= $b)."<br>";   //$a = $a | $b      //Bitwise Or
echo ($a ^= $b)."<br>"; //$a = $a ^ $b      // Bitwise Xor
echo ($a <<= $b)."<br>";  //$a = $a << $b     //Left shift
echo ($a >>= $b)."<br>"; //$a = $a >> $b     // Right shift

?>