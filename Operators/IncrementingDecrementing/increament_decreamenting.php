<?php
echo '== Alphabets ==' . PHP_EOL;
echo "<hr>";
$s = 'W';
for ($n=0; $n<6; $n++) {
    echo ++$s . PHP_EOL;
}
echo "<hr>";
// Digit characters behave differently
echo '== Digits ==' . PHP_EOL;
echo "<hr>";
$d = 'A8';
for ($n=0; $n<6; $n++) {
    echo $d++ . PHP_EOL;
}

echo "<hr>";
$d = 'A08';
for ($n=0; $n<6; $n++) {
    echo ++$d . PHP_EOL;
}
?>